/**
 * @desc 基于ElementUI Form rules 进行使用, 参考官方文档: http://element-cn.eleme.io/#/zh-CN/component/form [自定义校验规则]
 * @example  rules: {
 *  description: [
 *    {validator: this.$rules.checkNotBlank, trigger: 'blur'}
 *  ]
 * }
 *  import { paramRule } from "@/util/param-rules";
 *  rules: [
 *     { validator: paramRule.checkNotBlank, trigger: 'blur'}
 *  ]
 */
export const paramRule = {
  /**
   * 不能为空字符 缺陷描述：在标红*处输入空格，未显示“请输入非空格信息”
   */
  checkNotBlank(rule, value, callback) {
    let longrg = /[^\s]+$/;
    if (!longrg.test(value)) {
      callback(new Error('请输入非空格信息'));
    } else {
      callback()
    }
  },
}

